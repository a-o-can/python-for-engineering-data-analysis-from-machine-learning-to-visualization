This laboratory is taken at the Technical University of Munich, in 5th semester. The lab consists of various coding problems, which are to be solved by data analysis libraries as well as machine learning applications. 

The datasets, which have been used in the problems can be found in the "Datasets-20210616" folder. 
The solutions are to be found in "Codes" folders. 

An overview of the python libraries, which have been used during this lab, is as follows:
1) Pandas
2) Numpy
3) Matplotlib
4) scikit-learn
4) tensorflow.keras

An overview of the covered topic of the lab is as follows:
1) Getting around the Python ecosystem
2) Handling tabular data: Pandas
3) Plotting: Matplotlib
4) Basic (supervised) learning: (Linear) Regression and simple classification:
    Supervised Learning
    Regression
    Classification: KNN
    sklearn - basics
5) Regression techniques
6) Classification techniques
7) Neural Networks: tensorflow.keras
8) Unsupervised Learning
9) Clustering
10) Final Projects: Please take a look at the sentiment analysis project for this final project
    https://gitlab.com/a-o-can/sentiments-analysis

