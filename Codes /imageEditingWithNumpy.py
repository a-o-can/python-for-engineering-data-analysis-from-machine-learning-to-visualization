import numpy as np
from PIL import Image

im = Image.open('TUM_old.jpg')
im_array = np.array(im)
print(im_array.shape)

im_small = im_array[:,::3]
tum_small = Image.fromarray(im_small)
tum_small.save('TUM_small.png')

print(im_array.size)
tum_small.show()

im_array[:25,:] = np.array([48, 112, 179])
im_array[99:,:] = np.array([48, 112, 179])
im_array[:,:30] = np.array([48, 112, 179])
im_array[:,150:] = np.array([48, 112, 179])
Image.fromarray(im_array).show()
