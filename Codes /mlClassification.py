# Group: Mair, Marco; Can, Ali Oguz
import matplotlib.pyplot as plt
import numpy as np
from sklearn.datasets import load_iris
from matplotlib.colors import ListedColormap
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

# from sklearn.metrics import accuracy_score

cmap_light = ListedColormap(['orange', 'cyan', 'cornflowerblue'])
cmap_bold = ListedColormap(['darkorange', 'c', 'darkblue'])
# Read data
iris = load_iris(as_frame=True)
X = iris.data.iloc[:, :2].to_numpy()
Y = iris.target.to_numpy()

x_train, x_test, y_train, y_test = train_test_split(X, Y, shuffle=True, train_size=0.7)

# Grid Search Cross Val + KNN
param_knn = {'n_neighbors': np.arange(1, 50)}
gs_knn = GridSearchCV(KNeighborsClassifier(),
                      param_grid=param_knn, cv=5, n_jobs=-1, scoring='accuracy')
gs_knn.fit(x_train, y_train)
train_score_knn = gs_knn.best_score_
test_score_knn = gs_knn.score(x_test, y_test)
best_param_knn = gs_knn.best_params_
print('KNN: ', best_param_knn)

# GridSearchCV + linear SVC
param_lin = {'C': np.logspace(-3, 4, num=8), 'kernel': ['linear']}
gs_lin = GridSearchCV(SVC(kernel='linear', random_state=0, tol=1e-5),
                      param_grid=param_lin, n_jobs=-1, scoring='accuracy')
gs_lin.fit(x_train, y_train)
train_score_lin = gs_lin.best_score_
test_score_lin = gs_lin.score(x_test, y_test)
best_param_lin = gs_lin.best_params_
print('Linear SVC: ', best_param_lin)

# GridSearchCV + polynomial SVC
param_poly = {'C': np.logspace(-3, 3, num=7),
              'coef0': [0, 0.1, 1], 'degree': [4], 'kernel': ['poly']}
gs_poly = GridSearchCV(SVC(kernel='poly', random_state=0, gamma='auto'),
                       param_grid=param_poly, n_jobs=-1, scoring='accuracy')
gs_poly.fit(x_train, y_train)
train_score_poly = gs_poly.best_score_
test_score_poly = gs_poly.score(x_test, y_test)
best_param_poly = gs_poly.best_params_
print('Polynomial SVC: ', best_param_poly)

# GridSearchCV + rbf SVC
param_rbf = {'C': np.logspace(-3, 3, num=7), 'kernel': ['rbf']}
gs_rbf = GridSearchCV(SVC(kernel='rbf', gamma=0.7),
                      param_grid=param_rbf, n_jobs=-1, scoring='accuracy')
gs_rbf.fit(x_train, y_train)
train_score_rbf = gs_rbf.best_score_
test_score_rbf = gs_rbf.score(x_test, y_test)
best_param_rbf = gs_rbf.best_params_
print('Rbf SVC: ', best_param_rbf)

classes = ['Iris-Setosa', 'Iris-Versicolour', 'Iris-Virginica']


def plotSVC(title, model, plot_no, score1, score2, params):
    # create a mesh to plot in
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    h = (x_max / x_min) / 100
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    plt.subplot(2, 2, plot_no)
    Z = model.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    plt.contourf(xx, yy, Z, cmap=cmap_light, alpha=0.8)
    scatter = plt.scatter(x_train[:, 0], x_train[:, 1], c=y_train, cmap=cmap_light, edgecolor="black")
    plt.scatter(x_test[:, 0], x_test[:, 1], c=y_test, cmap=cmap_bold, marker='+', edgecolor="black")
    plt.xlim(xx.min(), xx.max())
    plt.title(title + ', train: {:.2f}, test: {:.2f}\n'.format(score1, score2) + str(params), fontsize=10)
    plt.legend(handles=scatter.legend_elements()[0], labels=classes)


plotSVC('KNN', gs_knn, 1, train_score_knn, test_score_knn, best_param_knn)
plotSVC('linear SVC', gs_lin, 2, train_score_lin, test_score_lin, best_param_lin)
plotSVC('poly SVC', gs_poly, 3, train_score_poly, test_score_poly, best_param_poly)
plotSVC('rbf SVC', gs_rbf, 4, train_score_rbf, test_score_rbf, best_param_rbf)
plt.show()
