# Week 52/3: 8.3 MNIST with a SVM
# 
# Group: Marco Mair, Ali Oguz Can
# https://www.tensorflow.org/quantum/tutorials/mnist
# https://www.tensorflow.org/datasets/keras_example
# https://www.tensorflow.org/tutorials/keras/classification

import os

import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow . keras import Model
from tensorflow . keras import Input

#enable or disable gpu; disabled because my CPU is somehow faster
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

if tf.test.gpu_device_name():
    print('GPU found')
else:
    print("No GPU found")

#version of tensorflow for cuda installation
print(tf.__version__)


#load data from tensorflow
(X_train, y_train), (X_test, y_test) = tf.keras.datasets.mnist.load_data()

# Rescale the images from [0,255] to the [0.0,1.0] range.
X_train = X_train/255.0
X_test = X_test/255.0
print("Finished normalizing data.")


#Create layers
model = tf.keras.models.Sequential([
  tf.keras.layers.Flatten(input_shape=(28, 28)),
  tf.keras.layers.Dense(500,activation='relu'),
  tf.keras.layers.Dense(128,activation='relu'),
  tf.keras.layers.Dense(10)
])


#Compile model
model.compile(
    optimizer=tf.keras.optimizers.Adam(0.001),
    loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
    metrics=[tf.keras.metrics.SparseCategoricalAccuracy()],
)


#Fit model and count time for that 
tic = time.perf_counter()
hist = model.fit(X_train,y_train,epochs=10)
toc = time.perf_counter()
print(f"Fitting took {toc - tic:0.4f} seconds.")

#print the Fitting History
#print(hist.history)

#show accuracy on testset
test_loss, test_acc = model.evaluate(X_test,  y_test, verbose=2)
print('\nTest accuracy:', test_acc)
#show accuracy on trainingset
train_loss, train_acc = model.evaluate(X_train,  y_train, verbose=2)
print('\nTrain accuracy:', train_acc)


#build a probability model for easier interpreting
probability_model = tf.keras.Sequential([model, tf.keras.layers.Softmax()])

predictions = probability_model.predict(X_test)



#Plotting the samples as well as converting into matrix
fig, axs = plt.subplots(nrows=4, ncols=5,
                        subplot_kw={'xticks': [], 'yticks': []})

#get length of index of predictions, set variables
length_index = len(predictions) 
stop = 0
false_pred = []

#for loop to get false predicted testsets
for index in range(length_index-1):          #go through 10000 values
  if np.argmax(predictions[index]) != y_test[index]:       #if predictions at position index is not the same as y_test label then:
    
    if stop < 20: 
      false_pred.append(index)
      np_false_pred = np.array(false_pred)

    if stop == 20:
      break
    stop = stop + 1


count = 0
#for loop for plots
for ax in axs.flat:
  if count == 20:
    break

  index = np_false_pred[count]

  pred = np.argmax(predictions[index])
  true_val = y_test[index]

  ax.imshow(X_test[index, :, :])

  ax.set_title(f'Pred: {pred}, True: {true_val}')
  count = count + 1


fig.suptitle(f'TOTAL TRAIN/TEST-ACCURACY: {train_acc:.3f}/{test_acc:.3f}')    
plt.tight_layout()
plt.show() 