import os
import matplotlib as mpl
import matplotlib.pyplot as plt


db = 'pressure_sensor.csv'
db_write = 'pressure_sensor_write.csv'
sum = 0
count = 0
mean = 0
values = []
avg_values = []

with open(db, 'r') as readfile:
    with open(db_write, 'w') as writefile:
        for line in readfile:
            if count == 0:
                writefile.write(line)
            if line.split() == []:
                continue
            if count in range(1,5):
                array = [float(i) for i in line.split()]
                sum += array[0]
                mean = sum/count
                print(mean)
                writefile.write('{:.5f}\n'.format(mean))
                values.append(array)
                avg_values.append(mean)
            count += 1
        
colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
plt.plot(range(len(values)),values, label='values', c=colors[0], marker = 'o')
plt.plot(range(len(avg_values)), avg_values, label='average values', c=colors[2], marker = 'o')
plt.legend()
plt.xlabel('Index')
plt.ylabel('Values')
plt.show()

