# https://scipy-lectures.org/intro/numpy/auto_examples/plot_mandelbrot.html
# Group: Marco Mair, Ali Oguz Can

import numpy as np
import matplotlib.pyplot as plt
from numpy import newaxis


def compute_mandelbrot(N_max, some_threshold, nx, ny):
    # A grid of c-values
    x = np.linspace(-2, 1, nx)
    y = np.linspace(-1.5, 1.5, ny)

    c = x[:, newaxis] + 1j * y[newaxis, :]

    # Mandelbrot iteration

    z = c

    # The code below overflows in many regions of the x-y grid, suppress
    # warnings temporarily
    with np.warnings.catch_warnings():
        np.warnings.simplefilter("ignore")
        for j in range(N_max):
            z = z ** 2 + c
        mandelbrot_set = (abs(z) < some_threshold)

    return mandelbrot_set


mandelbrot_set = compute_mandelbrot(50, 50., 800, 800)
fig = plt.figure()
axes = fig.add_subplot()
axes.imshow(mandelbrot_set.T, extent=[-2, 1, -1.5, 1.5], cmap='PuBuGn')
plt.axvline(x=0, lw=0.8)
plt.axhline(y=0, lw=0.8)
plt.title('Mandelbrot Set')
plt.xlabel('Re')
plt.ylabel('Im')
plt.show()
