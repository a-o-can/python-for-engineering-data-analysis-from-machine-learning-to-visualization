# Group: Marco Mair, Ali Oguz Can

import matplotlib.pyplot as plt
import pandas as pd

maxdic = {}

fig = plt.figure()
axes = fig.add_subplot()

for i in ['BY', 'BE', 'NW', 'SN', 'TH']:
    df = pd.read_csv(
        f'https://raw.githubusercontent.com/entorb/COVID-19-Coronavirus-German-Regions/master/data/de-states/de-state-{i}.tsv',
        sep='\t')
    axes.plot(df['Date'], df['Cases_Last_Week'], label=f'{i}')
    maxval = max(df['Cases_Last_Week'])
    idx = df[df['Cases_Last_Week'] == maxval].index
    maxdic.update({i: (maxval, df['Date'][idx].values[0])})

maxkey = max(maxdic, key=maxdic.get)  # calculate the key of maximum value in the dictionary
maxval = maxdic[maxkey][0]
maxdate = maxdic[maxkey][1]

axes.set_xticks(df.loc[::30, 'Date'])
plt.ylim(0.6, 5 * 10000)
plt.yscale('log')
plt.xlabel('Date')
plt.ylabel('n/week')
plt.title('7-day incidence of Covid-cases')
axes.grid(True)
plt.legend()
axes.set_facecolor((0.1, 0.3, 0.2))
axes.annotate(text=f'Maximum n={maxval} in {maxkey} \n@{maxdate}', xy=(maxdate, maxval), xytext=('2020-09-25', maxval),
              arrowprops={'arrowstyle': 'fancy'})
plt.show()
