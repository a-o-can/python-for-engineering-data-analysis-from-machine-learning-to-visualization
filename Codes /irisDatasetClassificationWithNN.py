# Group: Marco Mair, Ali Oguz Can
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap
from sklearn.preprocessing import StandardScaler
from tensorflow.keras import Model, Input
from tensorflow.keras.layers import Dense
from tensorflow.keras.optimizers import Adam
from sklearn.preprocessing import OneHotEncoder
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split

cmap_light = ListedColormap(['orange', 'cyan', 'cornflowerblue'])
cmap_bold = ListedColormap(['darkorange', 'c', 'darkblue'])
classes = ['Iris-Setosa', 'Iris-Versicolour', 'Iris-Virginica']

iris_data = load_iris(as_frame=True)
iris = iris_data.data.iloc[:, :2].to_numpy()
y = iris_data.target.to_numpy()

scaler = StandardScaler()
scaled_data = scaler.fit_transform(iris)

encoder = OneHotEncoder(sparse=False)
encoded_class = encoder.fit_transform(y.reshape(-1, 1))
x_train, x_test, y_train, y_test = train_test_split(scaled_data, encoded_class, train_size=0.7)

input_layer = Input(shape=2)
hidden_layer = Dense(units=8, activation='tanh')(input_layer)
output = Dense(units=3, activation='softmax')(hidden_layer)
model = Model(input_layer, output)
model.compile(loss='categorical_crossentropy', optimizer=Adam(), metrics=['accuracy'])  # sparse => integer (without onehotencoder)
hist1 = model.fit(x=x_train, y=y_train, epochs=300, validation_split=0.2)
predictions = model.predict(scaled_data)


input_layer2 = Input(shape=2)
hidden_layer21 = Dense(units=600, activation='relu')(input_layer2)
hidden_layer22 = Dense(units=100, activation='relu')(hidden_layer21)
output2 = Dense(units=3, activation='softmax')(hidden_layer22)
model2 = Model(input_layer2, output2)
model2.compile(loss='categorical_crossentropy', optimizer=Adam(), metrics=['accuracy'])  # sparse => integer (without onehotencoder)
hist2 = model.fit(x=x_train, y=y_train, epochs=300, validation_split=0.2)
predictions2 = model.predict(scaled_data)
print(hist2.history.keys())


def plotNN(act_func, model, plot_no, units, accuracy_train, accuracy_test, hist):
    # create a mesh to plot in
    x_min, x_max = iris[:, 0].min() - 1, iris[:, 0].max() + 1
    y_min, y_max = iris[:, 1].min() - 1, iris[:, 1].max() + 1
    h = (x_max / x_min) / 100
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    xx_inverse_train = scaler.inverse_transform(x_train)
    y_inverse_train = encoder.inverse_transform(y_train)
    x_test_inverse = scaler.inverse_transform(x_test)
    y_test_inverse = encoder.inverse_transform(y_test)
    #fig, ax = plt.subplots(1, 2)
    ax = plt.subplot(1, 2, plot_no)
    train_pred = encoder.inverse_transform(model.predict(scaler.transform(np.c_[xx.ravel(), yy.ravel()])))
    Z = train_pred.reshape(xx.shape)
    plt.contourf(xx, yy, Z, cmap=cmap_light, alpha=0.8)
    scatter = plt.scatter(xx_inverse_train[:, 0], xx_inverse_train[:, 1], c=y_inverse_train, cmap=cmap_light, edgecolor="black")
    ax.scatter(x_test_inverse[:, 0], x_test_inverse[:, 1], c=y_test_inverse, cmap=cmap_bold, marker='+', edgecolor="black")
    plt.xlim(xx.min(), xx.max())
    plt.ylim(yy.min(), yy.max())
    plt.title(f'{plot_no} ({units}) dense layers ({act_func}) final layer: softmax,'
              f' 300 epochs \n train_accuracy:{accuracy_train:.3f}, test_accuracy:{accuracy_test:.3f}', fontsize=10)
    plt.legend(handles=scatter.legend_elements()[0], labels=classes)
    axin1 = ax.inset_axes([0.01, 0.01, 0.2, 0.2])
    axin1.plot(np.arange(1, 301, 1), hist.history['loss'], color='blue', label='loss')
    axin1.plot(np.arange(1, 301, 1), hist.history['val_loss'], color='orange', linestyle='--', label='val_loss')
    axin1.set_xticks([])
    axin1.set_yticks([])

plotNN('tanh-activation', model, 1, 8, hist1.history['accuracy'][-1], hist1.history['val_accuracy'][-1], hist1)
plotNN('relu-activation', model2, 2, (600,100), hist2.history['accuracy'][-1], hist2.history['val_accuracy'][-1], hist2)

plt.show()

