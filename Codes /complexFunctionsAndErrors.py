def sum_or_product(*args, op, debug):
    sum = 0
    
    if op == '+':
        for num in args:
            sum += num
                
    elif op == '*':
        sum = sum + 1
        for num in args:
            sum *= num
                
    else:
        
        print(f'Error: Operation not supported: {op}')
        raise NotImplementedError

    if debug:
        string = [str(i) for i in args]
        print(f'{op.join(string)} = {sum}')
            
    elif debug == False:
        return sum

if __name__ == '__main__':

    try:
        print('Executing as main program')
        sum_or_product(5, 61, 8, 13, 42, op='+', debug=True)
        sum_or_product(5, 61, 8, 13, 42, op='*', debug=True)
        s = sum_or_product(5, 61, 8, 13, 42, op='+', debug=False)
        p = sum_or_product(5, 61, 8, 13, 42, op='*', debug=False)
        print(f'Sum: {s}, Product: {p}')
        sum_or_product(5, 61, 8, 13, 42, op='/', debug=True)
    except NotImplementedError:
        pass
        
