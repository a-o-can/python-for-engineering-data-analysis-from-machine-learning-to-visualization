import io
from collections import defaultdict


db=list(open('words.txt','r'))
dict = defaultdict(int)
count_hyphens = 0
count_1 = 0

for word in db:
    if 'nano' in word or word.startswith('sne'):
        print('@{} {}'.format(db.index(word),word))

#for word in db:
#    for char in word:
#        if char == '-':
#            dict[word] += 1
#    if dict[word] > 3:
#        count_hyphens += 1
#print('More than 3 hyphens: {}'.format(count_hyphens))

for word in db:
    count = 0
    for char in word:
        if char == '-':
            count += 1
    if count > 3:
        count_1 += 1

print('More than 3 hyphens: {}'.format(count_1))
        
max_length_word = max(db, key=len)
print('\nWord: {} with the length of {}'.format(max_length_word,len(max_length_word)))

