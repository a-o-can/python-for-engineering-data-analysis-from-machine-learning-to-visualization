#Group: Mair, Marco ; Can, Ali Oguz

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

db = 'polynomial.csv'

df = pd.read_csv(db)

x1 = df['x'].values  # numpy array of x values
x2 = x1 ** 3  # numpy array of x^3 values
target = df['y'].values  # numpy array of target values (y)
X = np.array([x1, x2]).T #X matrix
betaDach = np.dot(np.dot(np.linalg.inv(np.dot(X.T, X)), X.T), target) #predicted beta
print(betaDach)

xvalues = np.linspace(-10,10)
y = betaDach[0]*xvalues + betaDach[1]*xvalues**3
fig = plt.figure()
axes = fig.add_subplot()
plt.plot(xvalues, y, label = 'Fit Function')
axes.set_xlabel('x')
axes.set_ylabel('y')
axes.set_title('Regression Fit, a={:.6f}, b={:.6f}'.format(betaDach[0], betaDach[1]))
plt.scatter(x1, target, color = (0.8,0.3,0.1), label = 'Data Points')
plt.legend()
plt.show()
