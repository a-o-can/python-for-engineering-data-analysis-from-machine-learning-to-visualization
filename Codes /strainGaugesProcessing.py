import os
from math import sqrt

db = "strain_gauge_rosette.csv"
db_neu = open("strain_gauge_rosette_5rows.csv", "w")
db_neu_readonly = "strain_gauge_rosette_5rows.csv"

def e_min(arg1, arg2, arg3):
    return 0.5*(arg1+arg3-sqrt(2*((arg1-arg2)**2+(arg2-arg3)**2)))
    
def e_max(arg1, arg2, arg3):
    return 0.5*(arg1+arg3+sqrt(2*((arg1-arg2)**2+(arg2-arg3)**2)))


db_neu.write("R2_1 -m/m;R2_2 -m/m;R2_3 -m/m;e_1;e_2\n")
with open(db) as readfile:
    for index, line in enumerate(readfile):
        if index in range(1,5):
            array = [float(i) for i in line.split()]
#            a = array[0]
#            b = array[1]
#            c = array[2]
#            db_neu.write(f"{a};{b};{c};{e_min(a,b,c)};{e_max(a,b,c)}\n")
            db_neu.write("{:.7f};{:.7f};{:.7f};{:.7f};{:.7f}\n".format(array[0],array[1],array[2],e_max(*array),e_min(*array)))
    
db_neu.close()
with open(db_neu_readonly) as readfile:
    for index, row in enumerate(readfile):
        print(index+1,row)
