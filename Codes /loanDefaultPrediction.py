# Group: Marco Mair, Ali Oguz Can

from math import exp, prod
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, roc_curve, plot_confusion_matrix

file = 'sba_small.csv'
db = pd.read_csv(file)

# 2.1
fig = plt.figure()
ax1 = fig.add_subplot(1, 2, 1)
plt.hist(x=db[['CreateJob']], bins=20)
plt.xlabel('Created Job')
plt.title('Histogram of Default-column')
ax2 = fig.add_subplot(1, 2, 2)
plt.hist(bins=2, x=db[['Default']])
plt.xlabel('Default')
plt.title('Histogram of CreateJob-column')
plt.show()
# end

# 2.2
x_train = db[db['Selected'] == 0]
x_test = db[db['Selected'] == 1]
y_train = x_train['Default'].to_numpy()
y_test = x_test['Default'].to_numpy()

x_train.to_csv('sba_small_train.csv', index=False)
x_test.to_csv('sba_small_test.csv', index=False)
# end

# 2.3
# def logistic(features, coefficients):
#     return exp(coefficients[0] + np.dot(coefficients, features)) / (
#                 1 + exp(coefficients[0] + np.dot(coefficients, features)))

x_train_filter = x_train[['Recession', 'RealEstate', 'Portion']].to_numpy()
clf = LogisticRegression().fit(x_train_filter, db[db['Selected'] == 0]['Default'])
x_test_filter = x_test[['Recession', 'RealEstate', 'Portion']].to_numpy()
y_pred = clf.predict(x_test_filter)
score = clf.predict_proba(x_test_filter)

fpr, tpr, threshold = roc_curve(y_test, score[:, 1])
cmatrix = confusion_matrix(y_test, y_pred, labels=[0, 1])

plt.plot(fpr, tpr, label='ROC curve')
plt.plot([0, 1], [0, 1], linestyle='--')
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.legend()
plt.show()

plot_confusion_matrix(clf, x_test_filter, y_test, cmap=plt.cm.Blues, display_labels=[0, 1])
plt.title('Confusion Matrix')
plt.show()