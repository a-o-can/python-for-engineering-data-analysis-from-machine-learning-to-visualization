import pandas as pd
import matplotlib.pyplot as plt


db = 'pressure_sensor.csv'
db_df = pd.read_csv(db)

db_df_avg = db_df.rolling(5, min_periods = 1).mean()

db_df_avg.index = range(2, 2 +len(db_df_avg.index))
db_df_avg.to_csv('pressure_sensor_output.csv', float_format='%.5f', index_label = 1)

print('The dataframe has any empty values:', db_df_avg.isnull().values.any())

colors = plt.rcParams['axes.prop_cycle'].by_key()['color']
plt.plot(range(len(db_df)), db_df, label='values', c=colors[0], marker = 'o')
plt.plot(range(len(db_df_avg)), db_df_avg, label='average values', c=colors[2], marker = 'x')
plt.legend()
plt.xlabel('Index')
plt.ylabel('Values')
plt.show()
