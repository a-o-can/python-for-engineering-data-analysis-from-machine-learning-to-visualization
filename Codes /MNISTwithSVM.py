# Week 52/3: 8.3 MNIST with a SVM
# 
# Group: Marco Mair, Ali Oguz Can
# https://www.kaggle.com/nishan192/mnist-digit-recognition-using-svm/data?scriptVersionId=12859356
# CSVs from: http://makeyourownneuralnetwork.blogspot.com/2015/03/the-mnist-dataset-of-handwitten-digits.html


import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.preprocessing import scale
from sklearn import metrics
import matplotlib.pyplot as plt
from random import randrange

# reading the csv files using pandas
train_data = pd.read_csv('mnist_train.csv', header=None)
test_data = pd.read_csv('mnist_test.csv', header=None)
print("Finished reading data.")

# Seperating the X and Y variable
y = train_data.iloc[:, 0]
y_testdata = test_data.iloc[:, 0]

# Dropping the first variable from X 
X = train_data.drop(columns=0)
X_testdata = test_data.drop(columns=0)

# Normalization
X = X / 255.0
X_testdata = X_testdata / 255.0
print("Finished normalizing data.")

# scaling the features
X_scaled = scale(X)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X_scaled, y, test_size=0.2, train_size=0.8, random_state=10)

# model with optimal hyperparameters, hyperparameters from: https://www.kaggle.com/nishan192/mnist-digit-recognition-using-svm/data?scriptVersionId=12859356
model = SVC(C=10, gamma=0.001, kernel="rbf")

model.fit(X_train, y_train)
y_pred = model.predict(X_test)
y_pred_train = model.predict(X_train)
# metrics
accuracy_test = metrics.accuracy_score(y_test, y_pred)
accuracy_train = metrics.accuracy_score(y_train, y_pred_train)

# Framing, reindexing and renaming the y_test pd.Series
y_df = y_test.to_frame().reset_index().rename(columns={"index": "data_record", 0: "true_value"})

# Adding the prediction to a column to have better access to the data
y_df['prediction'] = pd.Series(y_pred)
print(y_df)

# Plotting the samples as well as converting into matrix
fig, axs = plt.subplots(nrows=4, ncols=5,
                        subplot_kw={'xticks': [], 'yticks': []})

# get length of index of y_df
length_df_index = len(y_df.index)

# for loop for plots
for ax in axs.flat:
    index = randrange(length_df_index - 1)
    print(f'Rand Index: {index}')

    pred = y_df.at[index, 'prediction']
    true_val = y_df.at[index, 'true_value']

    number = train_data.iloc[y_df.at[index, 'data_record'], 1:]
    # number.shape
    number = number.values.reshape(28, 28)
    ax.imshow(number)

    ax.set_title(f'Pred: {pred}, True: {true_val}')

fig.suptitle(f'TOTAL TRAIN/TEST-ACCURACY: {accuracy_train:.3f}/{accuracy_test:.3f}')
plt.tight_layout()
plt.show()
