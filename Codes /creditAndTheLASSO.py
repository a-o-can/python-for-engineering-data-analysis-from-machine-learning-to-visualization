#Group: Mair, Marco ; Can, Ali Oguz

import pandas as pd
import numpy as np
from sklearn.linear_model import Lasso
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
import matplotlib.pyplot as plt

db_csv = 'credit.csv'
db = pd.read_csv(db_csv, index_col=0)
y = np.array(db['Balance'])
X = np.empty((400, 4))
beta = []
r2score = []
a = []

for row in range(len(db)):
    for idx, column_name in enumerate(['Rating', 'Limit', 'Cards', 'Income']):
        X[row][idx] = db.loc[row + 1, column_name]

X_train, X_test, y_train, y_test = train_test_split(X, y, shuffle=True, train_size=0.8)
for i in range(0, 10000):
    model = Lasso(alpha=i)
    model.fit(X_train, y_train)
    beta.append(model.coef_)
    y_pred = model.predict(X_test)
    score = r2_score(y_pred, y_test)
    r2score.append(score)

beta = np.array(beta)
r2score = np.array(r2score)

fig = plt.figure()
axes1 = fig.add_subplot(2, 1, 1)
plt.xscale('log')

for idx, i in enumerate(['Rating', 'Limit', 'Cards', 'Income']):
    axes1.plot(range(10000), beta[:,idx], label = i)

axes1.legend()
plt.xlabel('alpha')
plt.ylabel('Value of the coefficient')

axes2 = fig.add_subplot(2, 1, 2)
axes2.plot(range(10000), r2score)
plt.xlabel('alpha')
plt.ylabel('R^2')
plt.xscale('log')
plt.show()