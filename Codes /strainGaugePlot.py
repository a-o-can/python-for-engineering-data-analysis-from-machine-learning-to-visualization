# Week47/Chapter 5-4: Redo the strain gauge exercise AGAIN
#  
# Group: Marco Mair, Ali Oguz Can

import os
import math
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import (inset_axes, InsetPosition,
                                                  mark_inset)
import matplotlib.projections

#used to get the current location of the script
__location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))

#Read data from file  "\\strain_gauge_rosette.csv" to get the correct path
data = pd.read_csv(__location__ + "/strain_ros.csv", sep=',', dtype={"R2_1-m/m": np.float64, "R2_2-m/m": np.float64, "R2_3-m/m" : np.float64})


#Columns a-c
a = data["R1_1-m/m"]
b = data["R1_2-m/m"]
c = data["R1_3-m/m"]


#(e_a-e_b)^2, (e_b-e_c)^2, arctan
square_ab = np.square(a-b)
square_bc = np.square(b-c)

arctan = np.arctan((2*b-a-c)/(a-c))


#e_1 = e min
data["e_1"] = 0.5 * (a + c + np.sqrt(2 * (square_ab + square_bc)))

#e_2 = e max
data["e_2"] = 0.5 * (a + c - np.sqrt(2 * (square_ab + square_bc)))

#calculate theta
data["theta"] = 0.5 * arctan

#strip time to be printable
pressure = data.loc[:,'WIKA_A10-V']
time = data.loc[:,'time'].str[7:]
time = time.str[:8]

fig = plt.figure()
axes = fig.add_subplot(facecolor=(.18, .31, .31))
plt.plot(time, pressure)
axes.set_xticks(time[::1000])
axes.set_xlabel('Time')
axes.set_ylabel('Pressure/Bar')
axes.set_ylim([0,250])
axes.set_title('Pressure and multiaxial strain')
plt.grid()

print(data)


#insets
for i in [30,100,330,400,480]:                                                         #cause of ticks 1:40 = ca. 100, 3:20 = ca. 200
    #to get the right data from the data points dp*10 
    dp = i*10


    axes_ins = inset_axes(axes, "100%", "100%",                                             # "100%" means to fill the bbox fully
                    loc='upper left',                                                       # location within the bounding box
                    bbox_transform =axes.transData,                                         # bounding box for the inset positioned by data - coordinates 
                    bbox_to_anchor =(i-30, 10+data.iat[dp,0] ,30 ,30),                         # bounding box of the inset x0 , y0 ( lower left corner ), width , heigth data.loc[i,'WIKA_A10-V']
                    
                    axes_class = matplotlib.projections.get_projection_class('polar'))      # use polar axes

    #To set the maximum radius plotted, remove radial and angle-marks and only do 2 gridlines, use (on the respective inset axes):
    rmax = 20
    axes_ins.set_rmax(rmax)
    axes_ins.set_rticks([])
    axes_ins.set_thetagrids ([0 , 90, 180 , 270 ,] , [])

    #plot Emin (theta=0, r=0) → (theta=theta, r=Emin); Emax (theta=0, r=0) → (theta=theta+pi/2, r=Emax)
    Emin = abs(data.at[dp, 'e_1'])
    Emax = abs(data.at[dp, 'e_2'])
    theta = data.at[dp, 'theta']
    theta_phase = data.at[dp, 'theta'] + math.pi / 2

    axes_ins = plt.plot( (0, theta), (0, Emin) )                 #EMIN BLAU
    axes_ins = plt.plot( (0, theta_phase), (0, Emax) )          #EMAX ORANGE

    #Print Emin = e_1 / Emax = e_2 / theta / theta +math.pi/2
    print(f'Emin = e_1 = {Emin} \t\t| Emax = e_2 = {Emax}')
    print(f'theta = {theta} \t\t| theta + math.pi/2 = {theta_phase}')

    #Arrow from xytext -> xy (datapoint)
    axes.annotate("", xy=(i, data.iat[dp, 0]), xytext=(i-15, 20+data.iat[dp, 0]), arrowprops=dict(arrowstyle="->"), xycoords=axes.transData)






print(axes_ins)
print(type(axes))

plt.show()
 


#Shift index from 0->1
#data.index += 1 


#used to get 7 decimal print output to show output beforehand
#pd.options.display.float_format = '{:.7f}'.format
#print(data)

#Output data from script
#data.to_csv(__location__ + "\\strain_ros_calculated.csv", sep=";", float_format='%.7f')
