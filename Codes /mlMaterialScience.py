"""Nitride Compounds"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.kernel_ridge import KernelRidge
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.model_selection import TimeSeriesSplit, KFold
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error

path = 'nitride_compounds.csv'
df = pd.read_csv(path)

x = df.iloc[:, 2:28].to_numpy()
y = df['HSE Eg (eV)'].to_numpy()

x_train, x_test, y_train, y_test = train_test_split(x, y, shuffle=True, train_size=0.8, random_state=42)
# print('x_train length:', len(x_train))
# -----------------------------------------------------------------

# ------------------------------------------------------------------
fractions = np.arange(0.1, 1, 0.1)
# mse = []
# r2 = []
# k_fold = KFold(n_splits=5, random_state=42, shuffle=True)
mse_all = [0] * len(fractions)
r2_all = [0] * len(fractions)
"""
tscv = TimeSeriesSplit(n_splits=10, test_size=0.2)
for train_index, test_index in tscv.split(x):
    X_train, X_test = x[train_index], x[test_index]
    Y_train, Y_test = y[train_index], y[test_index]
    model_KRR = KernelRidge(alpha=1.0)    
    model_KRR.fit(X_train, Y_train)
    y_pred = model_KRR.predict(X_test)
    mse.append(mean_squared_error(Y_test, y_pred))
    r2.append(r2_score(Y_test, y_pred))
mse_all[idx] = np.mean(mse)
r2_all[idx] = np.mean(r2)    
"""
"""
for idx, num in enumerate(fractions):
    num = int(len(x_train)*num)
    x_frac = x_train[:num]
    y_frac = y_train[:num]
    for train_indices, test_indices in k_fold.split(x_frac, y_frac):
        X_train, X_test = x_frac[train_indices], x_frac[test_indices]
        Y_train, Y_test = y_frac[train_indices], y_frac[test_indices]
        model_KRR = KernelRidge(alpha=1.0)    
        model_KRR.fit(X_train, Y_train)
        y_pred = model_KRR.predict(X_test)
        mse.append(mean_squared_error(Y_test, y_pred))
        r2.append(r2_score(Y_test, y_pred))
    mse_all[idx] = np.mean(mse)
    r2_all[idx] = np.mean(r2)
"""
for idx, num in enumerate(fractions):
    num = int(len(x_train) * num)
    x_frac = x_train[:num]
    y_frac = y_train[:num]
    lm_params = {'alpha': np.arange(1, 7), 'kernel': ['rbf', 'sigmoid', 'linear', 'laplacian']}
    hypermodel = GridSearchCV(KernelRidge(), lm_params, cv=5, scoring="neg_mean_squared_error")
    hypermodel.fit(x_frac, y_frac)
    y_pred = hypermodel.predict(x_test)
    mse_all[idx] = mean_squared_error(y_test, y_pred)
    r2_all[idx] = r2_score(y_test, y_pred)

# lm_params_2 = {'alpha': np.arange(1, 7)}
# hypermodel_2 = GridSearchCV(KernelRidge(), lm_params_2, cv=5, scoring="neg_mean_squared_error")
hypermodel.fit(x_train, y_train)
y_pred_test = hypermodel.predict(x_test)
y_pred_train = hypermodel.predict(x_train)
mae = mean_absolute_error(y_test, y_pred_test)
r2 = r2_score(y_test, y_pred_test)
print(mae, r2)

fig = plt.figure()
ax3 = fig.add_subplot(122)
ax3.scatter(y_test, y_pred_test, c='orange', label='test data')
ax3.scatter(y_train, y_pred_train, c='blue', label='training data')
ax3.plot([0, 6], [0, 6])
ax3.set_xlabel('Calculated gap')
ax3.set_ylabel('Model gap')
plt.xticks(np.arange(0, 7))
plt.title(f'Model R^2: {r2:.3f}, MAE: {mae:.3f}')

plt.legend()

ax1 = fig.add_subplot(121)
mse_curve, = ax1.plot(fractions, mse_all, 'b')
ax1.set_xlabel('fraction of training data used')
ax1.set_ylabel('MSE', color='b')
ax1.tick_params(axis='y', colors='blue')

plt.title('Learning curves')

ax2 = ax1.twinx()
r2_curve, = ax2.plot(fractions, r2_all, 'r')
ax2.set_ylabel('R^2', color='r')
ax2.tick_params(axis='y', colors='red')

plt.xticks(np.arange(0.2, 0.8, 0.2))
plt.show()
