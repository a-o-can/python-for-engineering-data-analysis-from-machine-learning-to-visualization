import pandas as pd
import numpy as np
from math import sqrt

db = 'strain_gauge_rosette.csv'
db_dataframe = pd.read_csv(db, sep = '\t')

def e_min(arg1, arg2, arg3):
    return 0.5*(arg1+arg3-sqrt(2*((arg1-arg2)**2+(arg2-arg3)**2)))
    
def e_max(arg1, arg2, arg3):
    return 0.5*(arg1+arg3+sqrt(2*((arg1-arg2)**2+(arg2-arg3)**2)))
    
col_names = ['R2_1 -m/m', 'R2_2 -m/m', 'R2_3 -m/m', 'e_1', 'e_2']
df_write = pd.DataFrame(columns = col_names)
df_write.to_csv('strain_gauge_rosette_output.csv')

for i in range(len(db_dataframe)):
    a = db_dataframe.iloc[i][0]
    b = db_dataframe.iloc[i][1]
    c = db_dataframe.iloc[i][2]
    e_1 = e_max(a, b, c)
    e_2 = e_min(a, b, c)
    df_append = pd.DataFrame(data = np.array([[a, b, c, e_1, e_2]]), columns = col_names)
    df_write = df_write.append(df_append, ignore_index = True)

df_write.index = range(2,len(db_dataframe) + 2)
df_write.to_csv('strain_gauge_rosette_output.csv', sep = ';', float_format='%.7f', index_label = 1)


print(df_write.head())
