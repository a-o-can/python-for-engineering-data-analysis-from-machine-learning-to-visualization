# Group: Marco Mair, Ali Oguz Can

import matplotlib.pyplot as plt
import numpy as np
from math import pi

fig = plt.figure()
axes1 = fig.add_subplot(1, 2, 1, projection='3d')
axes2 = fig.add_subplot(1, 2, 2, projection='3d')

def function(r):
    return -10 * abs(r) ** 2 + abs(r) ** 4


phi = np.array(np.linspace(0,2*pi))
r = np.linspace(0, 2.8)
Phi, R = np.meshgrid(phi,r)
Z = function(R)

X, Y = R*np.cos(Phi), R*np.sin(Phi)
axes1.plot_surface(X, Y, Z, cmap=plt.cm.YlGnBu_r)
axes2.plot_surface(X, Y, Z, cmap=plt.cm.YlGnBu_r, )
plt.suptitle('Mexican Hat')
axes1.view_init(20, 40)
plt.show()
