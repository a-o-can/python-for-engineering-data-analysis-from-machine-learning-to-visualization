#Group: Mair, Marco ; Can, Ali Oguz

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

db = pd.read_csv('breit_wigner.csv')

X = db['x']  # x values
G = db['g']  # g values
h = 0.001  # step size
B = [50000, 0.1, 0.1]  # starting values


def gfunc(x, b):  # g(x) function definition
    return b[0] / ((b[1] - x) ** 2 + b[2])


def jacobi(x, b):  # jacobi matrix with help of finite differences
    j1 = h / (((b[1] - x) ** 2) + b[2])
    j2 = b[0] / ((b[1] + h * 0.5 - x) ** 2 + b[2]) - b[0] / ((b[1] - h * 0.5 - x) ** 2 + b[2])
    j3 = b[0] / ((b[1] - x) ** 2 + b[2] + h * 0.5) - b[0] / ((b[1] - x) ** 2 + b[2] - h * 0.5)
    return np.array([j1 / h, j2 / h, j3 / h])


change = np.ones(len(B))  # append the change in it later on
lambda_start = np.linalg.norm(np.dot(jacobi(X, B).T, jacobi(X, B)))  # starting value of lambda
lam = lambda_start  # lam is an iterating variable where the start value is set to lambda_start

while change.all() > 0.001:  # change is a vector, hence if all values are greater than 0.01 then do, else break
    J_mat = np.array([jacobi(i, B) for i in np.array(X)])  # jacobi matrix for x values
    mat1 = np.dot(J_mat.T, J_mat)  # temp variable
    matrix = np.dot(np.linalg.inv(mat1 + lam * np.identity(len(B))),
                    J_mat.T)  # calculate the big term in front of the error vector
    G_Mat = np.array(gfunc(X, B))  # estimated g(x) values
    fehler = np.array(G - G_Mat)  # error
    delta_B = np.dot(matrix, fehler)  # calculate the delta B_i matrix
    B_next = B + delta_B  # calculate the delta B_i+1 matrix
    fehler_next = np.array(G - np.array(gfunc(X, B_next)))  # error_i+1
    fehler2 = np.dot(fehler.T, fehler)  # scalar error_i
    fehler_next2 = np.dot(fehler_next.T, fehler_next)  # scalar error_i+1
    rho = (fehler2 - fehler_next2) / np.dot(delta_B.T,
                                            (lam * delta_B + np.dot(J_mat.T, fehler)))  # parameter rho to change lambda
    if rho >= 0.75:
        lam = lam / 3
    elif 0 < rho <= 0.25:
        lam = 2 * lam
    elif 0.25 < rho < 0.75:
        lam = lam
    else:
        break
    change = B_next / B  # calculate the change
    B = B_next  # set the current value of B to the next iteration

fig = plt.figure()
axes = fig.add_subplot()
plt.scatter(X, G, color='red', label='Data')
values = np.linspace(0, 200, num=200)
plt.plot(values, [gfunc(i, [B[0], B[1], B[2]]) for i in values], label='function g(x)')
plt.legend()
plt.title(f'Breit-Wigner function for params a={B[0]}, b={B[1]}, c={B[2]}')
plt.show()
