# Group: Mair, Marco ; Can, Ali Oguz

import pandas as pd
import numpy as np
from sklearn.kernel_ridge import KernelRidge
from sklearn.model_selection import GridSearchCV, train_test_split
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
import matplotlib.pyplot as plt

db = pd.read_csv('wave.csv')
X = db['x'].to_numpy()
y = db['y'].to_numpy()
X_train, X_test, y_train, y_test = train_test_split(X, y, shuffle=True, train_size=0.8)


def f(x):
    return np.exp(-((x / 4) ** 2)) * np.cos(4 * x)


model = KernelRidge(kernel='rbf')
krr_params = {'alpha': [1e0, 1e-1, 1e-2, 1e-3],
              'gamma': np.logspace(-2, 2, 5)}
hypermodel = GridSearchCV(model, krr_params, cv=5, scoring='neg_mean_squared_error')
hypermodel.fit(X_train.reshape(-1, 1), y_train)
params = hypermodel.best_params_

y_pred = hypermodel.predict(X_test.reshape(-1, 1))
mse = mean_squared_error(y_pred, y_test)
r2 = r2_score(y_pred, y_test)
mae = mean_absolute_error(y_pred, y_test)

fig = plt.figure()
axes = fig.add_subplot()
plt.scatter(X_train, y_train, label='training data')
plt.scatter(X_test, y_test, label='test data')
plt.plot(np.arange(-10, 10, 0.01), f(np.arange(-10, 10, 0.01)), label='f')
plt.plot(np.arange(-10, 10, 0.01), hypermodel.predict(np.arange(-10, 10, 0.01).reshape(-1, 1)), label='predicted f')
plt.legend()
plt.title(f'MSE:{mse}, MAE:{mae}, R^2:{r2}')
plt.show()

# The data is noisy, so the noise has to be overcome. Therefore one can scale the data in order to reduce the noise.
# For this purpose, we could use standardization. This removes the mean of a feature and divides by the standard
# deviation. If the feature values are normally distributed, we'll get a Gaussian, which is centered around zero with
# a variance of one. If the feature values aren't normally distributed, we can remove the median and divide by the
# interquartile range. The interquartile range is a range between the first and third quartile (or 25th and 75th
# percentile). Scaling features to a range is a common choice of range between zero and one.
