# Week 52/3: 9.3 NN as an universal function approximator
# 
# Group: Marco Mair, Ali Oguz Can

import os

import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
from sklearn.preprocessing import MinMaxScaler
from tensorflow . keras import Model
from tensorflow . keras import Input

#enable or disable gpu; disabled because my CPU is somehow faster
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

if tf.test.gpu_device_name():
    print('GPU found')
else:
    print("No GPU found")

#version of tensorflow for cuda installation
print(tf.__version__)


#creating sine numpy array
x_sin = np.linspace(-np.pi, np.pi, 400)
y_sin = np.sin(x_sin)


# reshape arrays into into rows and cols
x_sin = x_sin.reshape((len(x_sin), 1))
y_sin = y_sin.reshape((len(y_sin), 1))


# separately scale the input and output variables with MinMaxScaler
scale_x = MinMaxScaler()
x_sin = scale_x.fit_transform(x_sin)
scale_y = MinMaxScaler()
y_sin = scale_y.fit_transform(y_sin)
print(x_sin.min(), x_sin.max(), y_sin.min(), y_sin.max())


#Create layers
model = tf.keras.models.Sequential([
  tf.keras.layers.Dense(32, input_dim=1,activation='relu'),
  tf.keras.layers.Dense(18,activation='relu'),
  tf.keras.layers.Dense(1)
])


# define the loss function and optimization algorithm
model.compile(
    optimizer=tf.keras.optimizers.Adam(0.01),
    loss='mae',
    metrics=['accuracy', 'mae'],
)
#model.compile(loss='mse', optimizer='adam')


#Plotting the samples as well as converting into matrix
fig, axs = plt.subplots(nrows=3, ncols=3)


# fit the model on the training dataset
i=1
epo = 0

tic_total = time.perf_counter()
for ax in axs.flat:
  epo += 75

  tic = time.perf_counter()
  model.fit(x_sin, y_sin, epochs=epo, verbose=0)
  toc = time.perf_counter()
  print(f"Fitting took {toc - tic:0.4f} seconds after {epo} epochs")


  # make predictions for the input data
  y_predict = model.predict(x_sin)

  # get mae score
  mae = tf.keras.losses.MeanAbsoluteError()
  mae_loss = mae(y_sin, y_predict)


  # inverse transforms
  x_plot = scale_x.inverse_transform(x_sin)
  y_plot = scale_y.inverse_transform(y_sin)
  y_predict_plot = scale_y.inverse_transform(y_predict)

  ax.plot(x_plot, y_plot)
  ax.plot(x_plot, y_predict_plot)
  ax.set_title(f'after {epo} epochs; MAE: {mae_loss:0.3f}')
 
toc_total = time.perf_counter()
print(f"Total fitting took {toc_total - tic_total:0.4f} seconds after {epo} epochs")

plt.tight_layout()
plt.show() 