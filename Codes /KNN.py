#Group: Mair, Marco; Can, Ali Oguz

from matplotlib.ticker import MultipleLocator
from sklearn import datasets, neighbors
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import ListedColormap

iris = datasets.load_iris(as_frame=True)
k_neighbors = [1, 5, 10, 50]
cmap_light = ListedColormap(['orange', 'cyan', 'cornflowerblue'])
cmap_bold = ListedColormap(['darkorange', 'c', 'darkblue'])
X = iris.data.iloc[:, :2]
y = iris.target

x_min, x_max = X.iloc[:, 0].min() - 1, X.iloc[:, 0].max() + 1
y_min, y_max = X.iloc[:, 1].min() - 1, X.iloc[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, .02),
                         np.arange(y_min, y_max, .02))

bib = {0: 'Iris-Setosa', 1 : 'Iris-Versicolour', 2 : 'Iris-Virginica'}


fig = plt.figure() #on one figure
for idx, i in enumerate(k_neighbors):
    axes = fig.add_subplot(2, 2, idx + 1)
    clf = neighbors.KNeighborsClassifier(n_neighbors=i, weights='uniform')
    clf.fit(iris['data'].iloc[:, :2], iris.target)
    plt.title(f'{i}-nearest-neighbors')
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    plt.pcolormesh(xx, yy, Z, cmap=cmap_light, shading='auto')
    for j in bib:
        plt.scatter(iris['data'][iris['target'] == j].iloc[:, 0], iris['data'][iris['target'] == j].iloc[:, 1],
                    marker='x', cmap=cmap_light, edgecolors='k', label=bib[j])
    plt.legend()
    axes.xaxis.set_major_locator(MultipleLocator(0.3))
    axes.yaxis.set_major_locator(MultipleLocator(0.3))
    axes.grid(which='major')


plt.show()

