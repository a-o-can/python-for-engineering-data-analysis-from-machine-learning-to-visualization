import os
import json
import numpy as np
from collections import defaultdict

path_to_tas_data = '/Users/alioguzcan/Desktop/Python_Lab/week2/tas_data'

d = {} #defaultdict(float)
db = os.listdir('tas_data')

for file in db:
    os.chdir(path_to_tas_data)
    with open(file,'r') as db_file:
        key = file.split('.')[0]
        array = list(db_file.read().split())
        d[key] = [float(i) for i in array]

os.chdir('/Users/alioguzcan/Desktop/Python_Lab/week2/')
with open('raw_data.json', 'w') as jsonfile:
    json.dump(d,jsonfile)

numpy_array = np.array(list(d.items()))

d2 = defaultdict(float)
time_period = 2012 - 1901 + 1

#k = 0
#for i in numpy_array[:,0]:
#    d2[i] = {
#            't_avg': sum(numpy_array[k,1]) / time_period,
#            't_max_time': numpy_array[k,1].index(np.max(numpy_array[k,1])) + 1901,
#            't_max': np.max(numpy_array[k,1]),
#            't_min_time': numpy_array[k,1].index(np.min(numpy_array[k,1])) + 1901,
#            't_min': np.min(numpy_array[k,1])
#            }
#    k = k + 1

for index, country in enumerate(numpy_array[:,0]):
    d2[country] = {
            't_avg': sum(numpy_array[index,1]) / time_period,
            't_max_time': numpy_array[index,1].index(np.max(numpy_array[index,1])) + 1901,
            't_max': np.max(numpy_array[index,1]),
            't_min_time': numpy_array[index,1].index(np.min(numpy_array[index,1])) + 1901,
            't_min': np.min(numpy_array[index,1])
                }

with open('aggregated_data.json', 'w') as jsonfile:
    json.dump(d2,jsonfile)

print(d['DNK']) #raw data print
print(d2['CHN']) #for the aggregated results
print(d2['DNK']) #for the aggregated results
print(d2['BRA']) #for the aggregated results
print(d2['PAK']) #for the aggregated results
print(d2['CAN']) #for the aggregated results
print(d2['CIV']) #for the aggregated results

