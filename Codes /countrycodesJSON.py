import json
import math

import matplotlib.pyplot as plt
import pandas as pd

#3.1
db = 'countrycodes.json'
#df_json = pd.read_json(db, orient = 'index')
#print(df_json)
with open(db) as db_open:
    db_json = json.load(db_open)
df_json = pd.DataFrame.from_records(db_json)
#pprint.PrettyPrinter(indent = 4).pprint(df_json)
df_json = df_json.T
df_json.to_csv('countrycodes.csv')
#3.1

#3.2
#ISO = ['AUS', 'DEU', 'GEO', 'IND', 'ZAF', 'ISR', 'PHL', 'USA', 'BRA']
ISO = df_json.index.to_numpy()
df_master = pd.DataFrame(index = range(0,112))
url_DEU = f'http://climatedataapi.worldbank.org/climateweb/rest/v1/country/cru/tas/year/DEU.json'
df = pd.read_json(url_DEU)
df_master.index = df['year']

i = 0

for country in ISO:
    url_tmp = f'http://climatedataapi.worldbank.org/climateweb/rest/v1/country/cru/tas/year/{country}.json'
    url_pr = f'http://climatedataapi.worldbank.org/climateweb/rest/v1/country/cru/pr/year/{country}.json'
    df_tmp = pd.read_json(url_tmp)
    #print(df_tmp)
    df_master.insert(loc = i, column = f'{country} - T', value = df_tmp['data'].values)
    df_pr = pd.read_json(url_pr)
    #print(df_pr)
    df_master.insert(loc = i+1, column = f'{country} - pr', value = df_pr['data'].values)
    i = i + 2

df_master.to_csv('base_data.csv')
#3.2

#3.3
df_minT = df_master.T[::2].idxmin()
emptyseries = pd.Series(index=df_minT.index, dtype = 'float64')
df_end = pd.concat([df_minT, emptyseries], axis=1)

df_minPr = df_master.T[1::2].idxmin()
emptyseriesPr = pd.Series(index=df_minPr.index, dtype = 'float64')
df_endPr = pd.concat([df_minPr, emptyseriesPr], axis = 1)

for year in df_minT.index[:]:
    value = df_master.loc[year, df_minT.loc[year]]
    df_end.loc[year, 1] = value
    df_end.loc[year, 0] = df_end.loc[year, 0].split(sep = ' ')[0]

for year in df_minPr.index[:]:
    value = df_master.loc[year, df_minPr.loc[year]]
    df_endPr.loc[year, 1] = value
    df_endPr.loc[year, 0] = df_endPr.loc[year, 0].split(sep = ' ')[0]

df_lowest = pd.concat([df_end, df_endPr] , axis=1, ignore_index = True)
df_lowest.to_csv('lowest.csv')
#3.3

#3.4
df_bool = df_master[df_master.loc[:, ::2] > 10]
for country in df_bool.columns[::2]:
    var = country.split(sep = ' ')[0]
    print(f'{var}: ', df_bool[country].count())
#3.4

#3.5
#DEU and USA
ax = df_master.plot(y = 'DEU - T', label = 'DEU - T')
df_master.plot(y = 'USA - T', label = 'USA - T', ax = ax)
plt.xlim((1920, 1947))
plt.legend()
plt.title('Average Temperature')
plt.ylabel('Temperature Values')

ax = df_master.plot(y = 'DEU - pr', label = 'DEU - pr')
df_master.plot(y = 'USA - pr', label = 'USA - pr', ax = ax)
plt.xlim((1920, 1947))
plt.legend()
plt.title('Precipitation')
plt.ylabel('Precipitation Values')
plt.show()
#3.5

#3.6
df_decade = df_master[['USA - T','DEU - T']]
grp = df_decade.groupby(by = lambda year: math.floor(year / 10))
idx = []
max_temp = []
min_temp = []
for name, group in grp:
    name = f'{group.index[0]} - {group.index[-1]}'
    idx.append(name)
    for i in ['USA - T','DEU - T']:
        max_temp.append(group[i].max())
        min_temp.append(group[i].min())
    
iterables=[idx,['USA - T', 'DEU - T']]
multiindex = pd.MultiIndex.from_product(iterables, names=['decades', 'countries'])
df_extreme = pd.DataFrame(data = {'Max - T': max_temp, 'Min - T': min_temp}, index = multiindex)
print(df_extreme)
df_extreme.to_csv('extremeT.csv')

#print(df_extreme.index.get_level_values(0)[::2])
df_extreme[df_extreme.index.get_level_values(1) == 'DEU - T'][['Max - T', 'Min - T']].hist(bins = 5)
df_extreme[df_extreme.index.get_level_values(1) == 'USA - T'][['Max - T', 'Min - T']].hist(bins = 5)
plt.show()
#3.6
